﻿using System;
using System.Threading;
using System.IO;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Diagnostics.Contracts;
using System.Text.RegularExpressions;

namespace evelyn_coordinate_sorter
{

    class Program
    {
        public struct Vector3
        {
            public double x;
            public double y;
            public double z;
            public Vector3(double x, double y, double z)
            {
                this.x = x;
                this.y = y;
                this.z = z;
            }
            public double CalculateDistanceToVector(Vector3 p0)
            {
                return Math.Abs(Math.Sqrt(Math.Pow((p0.x - this.x), 2) + Math.Pow((p0.y - this.y), 2) + Math.Pow((p0.z - this.z), 2)));
            }
        };

        static bool getLocation()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("-------------  Created by Evelyn#1571  -------------");
            Console.WriteLine("-------  This software is under MIT license  -------");
            Console.WriteLine("----------------------------------------------------\n\n");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Enter folder directory containing the files: ");
            Console.ResetColor();
            string sFolderPath = Console.ReadLine();
            
            if (Directory.Exists(sFolderPath))
            { 
                string[] sFileNames = { };
                Vector3[] v3Coords = { };
                int iCount = 0;
                bool allGood = true;

                if (!sFolderPath.EndsWith("/")) { sFolderPath = sFolderPath + "/"; }

                foreach (string file in Directory.EnumerateFiles(sFolderPath, "*.txt"))
                {
                    string contents = File.ReadAllText(file);
                    string fileName = Path.GetFileName(file);
                    string[] point;
                    double x, y, z;
                    try
                    {
                        point = contents.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                        x = Convert.ToDouble(point[0]);
                        y = Convert.ToDouble(point[1]);
                        z = Convert.ToDouble(point[2]);
                    } catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Location you're providing was not correct, or do you have some other unrelated files in here?\n");
                        Console.WriteLine("{0} Exception caught.", e);
                        Console.ResetColor();
                        return false;
                    }
                    Array.Resize(ref sFileNames, sFileNames.Length + 1);
                    Array.Resize(ref v3Coords, v3Coords.Length + 1);
                    sFileNames[sFileNames.GetUpperBound(0)] = fileName;
                    v3Coords[v3Coords.GetUpperBound(0)] = new Vector3(x, y, z);
                    Console.WriteLine($"Teleportation file detected - {sFileNames[iCount]} | assigned as ID {iCount}\n{x} | {y} | {z}");
                    iCount += 1;
                    
                }

                Console.WriteLine("\n");

                double[] GetClosestCoordinateIndex(int index, int[] ignore)
                {
                    double distance = 9999.0;
                    int closestIndex = -1;
                    for (int i=0; i < iCount; i++)
                    {
                        if (i != index)
                        {
                            double dist = v3Coords[index].CalculateDistanceToVector(v3Coords[i]);
                            if (distance > dist)
                            {
                                bool ignored = false;
                                for (int d=0; d < ignore.Length; d++)
                                {
                                    if (i == ignore[d]) { ignored = true; }
                                }
                                if (!ignored)
                                {
                                    closestIndex = i;
                                    distance = dist;
                                }
                            }
                        }
                    }
                    if (closestIndex == -1)
                    {
                        return new double[] { (double)closestIndex, 0D, 0D, 0D, 0D };
                    }
                    return new double[] { (double)closestIndex, distance, v3Coords[closestIndex].x, v3Coords[closestIndex].y, v3Coords[closestIndex].z };
                }

                if (iCount == 0)
                {
                    // no files inside the folder
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nAre you sure this is the correct directory? There are no teleportation files inside this folder (.txt)");
                    Console.ResetColor();
                    return false;
                }
                else
                {
                    bool didBackup = false;
                    int tc = 0;
                    int cur = 0;
                    int[] blacklist = { };
                    int[] orderCoords = { cur };
                    Vector3[] orderVect = { v3Coords[0] };
                    do
                    {
                        int oldcur = cur;
                        double[] result = GetClosestCoordinateIndex(cur, blacklist);
                        
                        Array.Resize(ref orderCoords, orderCoords.Length + 1);
                        Array.Resize(ref blacklist, blacklist.Length + 1);
                        Array.Resize(ref orderVect, orderVect.Length + 1);
                        orderCoords[orderCoords.GetUpperBound(0)] = (int)result[0];
                        blacklist[blacklist.GetUpperBound(0)] = oldcur;
                        orderVect[orderVect.GetUpperBound(0)] = new Vector3(result[2], result[3], result[4]);
                        cur = (int)result[0];
                        if (cur == -1) { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine($"\n\nSorting finished"); Console.ResetColor(); }
                        else { Console.WriteLine($"Closest from ID:{oldcur} is ID:{cur} at a distance of {result[1]} Meters"); }
                        tc++;
                    } while (tc < iCount);

                    Console.WriteLine("\n");

                    if (allGood)
                    {
                        ConsoleKeyInfo confBackup;
                        do
                        {
                            Console.WriteLine("Do you want to create a Backup? [Y/N]");
                            while (Console.KeyAvailable == false)
                            {
                                Thread.Sleep(250);
                            }
                            confBackup = Console.ReadKey(true);
                            if (confBackup.Key == ConsoleKey.Y)
                            {
                                didBackup = true;
                                foreach (string fileName in sFileNames)
                                {
                                    string src = sFolderPath + fileName;
                                    string dest = sFolderPath + "Backup/" + fileName;
                                    try
                                    {
                                        if (!Directory.Exists(sFolderPath + "Backup/"))
                                        {
                                            Directory.CreateDirectory(sFolderPath + "Backup/");
                                        }
                                        System.IO.File.Move(src, dest);
                                        Console.WriteLine("Backup created " + dest);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("{0} Exception caught.", e);
                                        return false;
                                    }
                                }
                            }
                            else if (confBackup.Key == ConsoleKey.N)
                            {
                                foreach (string fileName in sFileNames)
                                {
                                    string src = sFolderPath + fileName;
                                    try
                                    {
                                        if (File.Exists(Path.Combine(sFolderPath, fileName)))
                                        {
                                            File.Delete(Path.Combine(sFolderPath, fileName));
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("{0} Exception caught.", e);
                                        return false;
                                    }
                                }
                            }
                        } while (confBackup.Key != ConsoleKey.Y && confBackup.Key != ConsoleKey.N);

                    }

                    Console.WriteLine("\n");

                    for (int i=0; i < orderCoords.Length; i++)
                    {
                        int number = orderCoords[i];
                        
                        if (number != -1)
                        {
                            Vector3 thisVect = orderVect[i];
                            string originName = sFileNames[number];

                            //Console.WriteLine($"Writing {number} at order {i} with name {originName}");
                            string cfileName = i.ToString();
                            if (i < 10)
                            {
                                cfileName = "000" + cfileName;
                            }
                            else if (i < 100)
                            {
                                cfileName = "00" + cfileName;
                            }
                            else if (i < 1000)
                            {
                                cfileName = "0" + cfileName;
                            }
                            originName = Regex.Replace(originName, @"[\d-]", string.Empty).Replace(".txt", string.Empty);
                            string finalName = $"{cfileName} - {originName}.txt";
                            string pathString = System.IO.Path.Combine(sFolderPath, finalName);
                            Console.WriteLine($"Writing {finalName}");
                            if (!System.IO.File.Exists(pathString))
                            {
                                using (System.IO.FileStream fs = System.IO.File.Create(pathString))
                                {
                                    var data = thisVect.x.ToString() + "\n" + thisVect.y.ToString() + "\n" + thisVect.z.ToString();
                                    byte[] bytes = Encoding.UTF8.GetBytes(data);
                                    fs.Write(bytes, 0, bytes.Length);
                                }
                            }
                            else
                            {
                                Console.WriteLine("File \"{0}\" already exists.", finalName);
                            }
                        }

                    }

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"\n\nOperation Completed!");
                    Console.ResetColor();

                    Console.ForegroundColor = ConsoleColor.Red;
                    if (didBackup) { Console.WriteLine("\nIf anything bad happen you can find your backup at " + sFolderPath + "Backup/"); }
                    Console.WriteLine("If you encounter any bug free to message me on discord Evelyn#1571");
                    Console.ResetColor();
                }

                Console.WriteLine("Press ESC to Exit");
                ConsoleKeyInfo ckiKeyPressed;
                do
                {
                    while (Console.KeyAvailable == false)
                    {
                        Thread.Sleep(250);
                    }
                    ckiKeyPressed = Console.ReadKey(true);
                } while (ckiKeyPressed.Key != ConsoleKey.Escape);
                return true;
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nError location not found");
            Console.ResetColor();
            return false;


        }
        static void Main(string[] args)
        {
                while (!getLocation())
                {
                    Console.WriteLine("\nPress Any key to retry\nPress ESC to Exit");
                    while (Console.KeyAvailable == false)
                    {
                        Thread.Sleep(250);
                    }
                    ConsoleKeyInfo ckiKeyPressed = Console.ReadKey(true);
                    if (ckiKeyPressed.Key == ConsoleKey.Escape)
                    {
                        return;
                    }
                    Console.Clear();
                }
        }
    }
}
